<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ser="http://services.api.ycs.com" xmlns:xsd="http://model.api.ycs.com/xsd">
<soap:Header/>
<soap:Body>
   <ser:serviceCall>
      <ser:naradaRequest>
         <xsd:requestBody>
            <xsd:request><?php echo $body?></xsd:request>
         </xsd:requestBody>
         <xsd:requestHeader>
            <xsd:apiKey><?php echo $api?></xsd:apiKey>
            <xsd:apiPasswd><?php echo $password?></xsd:apiPasswd>
            <xsd:apiUserId><?php echo $user_id?></xsd:apiUserId>
            <xsd:channelType>API</xsd:channelType> 
            <xsd:clientId><?php echo $client_id?></xsd:clientId> 
            <xsd:msgRefNo><?php echo $msg_ref_no?></xsd:msgRefNo> 
            <xsd:msgTimeStamp><?php echo date('YmdHis')?></xsd:msgTimeStamp>
            <xsd:sessionKey><?php echo $session?></xsd:sessionKey>
            <xsd:token><?php echo $token?></xsd:token>
         </xsd:requestHeader>
      </ser:naradaRequest>
   </ser:serviceCall>
</soap:Body>
</soap:Envelope>