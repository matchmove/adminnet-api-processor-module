<?php

namespace App\Modules\Processor\Tests;

use App\Modules\Core\tests\BaseTest;

/**
 * @group processor
 */
class ModelAPACBLTest extends BaseTest
{

    /**
     * Setup the test case
     */
    public function setUp()
    {
        parent::setUp();
    }
    
    /**
     * Test send 
     *
     */
    public function testSend()
    {
        $apiReturnArr = array('TxnRefNo' => '1234565');
        
        $apacbl = $this->getMockBuilder('Processor_YCS_APACBL')
            ->setMethods(array('send'))
            ->disableOriginalConstructor()
            ->getMock();
        
        $apacbl->_configuration_group = 'mcimbacard';
        $card = new \stdClass();
        $card->proxy_number = '0003434';
        
        $apacbl->expects($this->any())
            ->method('send')
            ->will($this->returnValue($apiReturnArr));
        
        $this->assertArrayHasKey('TxnRefNo', $apiReturnArr);
    }
}