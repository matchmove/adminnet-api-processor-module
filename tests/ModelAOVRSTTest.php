<?php

namespace App\Modules\Processor\Tests;

use App\Modules\Core\tests\BaseTest;

/**
 * @group processor
 */
class ModelAOVRSTTest extends BaseTest
{

    /**
     * Setup the test case
     */
    public function setUp()
    {
        parent::setUp();
    }
    
    /**
     * Test send 
     *
     */
    public function testSend()
    {
        $apiReturnArr = array('TxnRefNo' => '1234565');
        
        $aovrst = $this->getMockBuilder('Processor_YCS_AOVRST')
            ->setMethods(array('product_code', 'campaign_code', 'send'))
            ->disableOriginalConstructor()
            ->getMock();

        $aovrst->expects($this->any())
            ->method('product_code')
            ->will($this->returnValue('234324'));
        
        $aovrst->expects($this->any())
            ->method('campaign_code')
            ->will($this->returnValue('123432'));
        
        $aovrst->_configuration_group = 'mcimbacard';
        $card = new \stdClass();
        $card->proxy_number = '0003434';
        
        $aovrst->expects($this->any())
            ->method('send')
            ->will($this->returnValue($apiReturnArr));
        
        $this->assertArrayHasKey('TxnRefNo', $apiReturnArr);
    }
}