<?php 
namespace App\Modules\Processor\Models;

use App\Modules\Core\Helpers\Logger;
use Exception;

class Processor
{
    /**
     * Function will get ycs config based on the card type 
     * @param string $subgroup
     * @param string $group
     * @return array
     * @throws Exception
     */
    public static function config($subgroup, $group = 'processor')
    {
        $config = config($group.'.'.$subgroup);
        
        if (!empty($config))
        {
            return $config;
        }
        
        Logger::create('PROCESSOR/YCS/PROCESSOR/CONFIG', [
           'Config '.$group.'.'.$subgroup.' not declared'
        ]);
        // in case of empty config throw exception
        throw new Exception('Config '.$group.'.'.$subgroup.' not declared');
    }
}