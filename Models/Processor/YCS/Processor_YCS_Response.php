<?php 
namespace App\Modules\Processor\Models\Processor\YCS;

use App\Modules\Processor\Models\Processor\Response\Processor_Response_XML;
use App\Modules\Core\Helpers\HtmlCompress;
use Exception;
use App\Modules\Core\Helpers\Logger;

class Processor_YCS_Response
{
    /* Constant for API status */
    const HTTP_OK = 200;
    const HTTP_NOT_FOUND = 404;
    
    /* Constant for API response time */
    const DURATION_TAG_500MS = '500MS';
    const DURATION_TAG_1SEC = '1SEC';
    const DURATION_TAG_3SEC = '3SEC';
    const DURATION_TAG_5SEC = '5SEC';
    
    const UNIT_MILLISECONDS = 'Milliseconds';
    
    /* Constant for API message */
    const API_ERROR_OK_CODE = '00';
    const API_ERROR_OK_MESSAGE = 'Transaction Success';
    
    protected $_curl_headers = array(
        'Content-type: application/soap-xml;charset="utf-8"',
        'Accept: text/xml'
    );
    
    protected $_data = null;
    protected $_api = null;
    protected $_encrypted = true;
    
    protected $_http_code = Processor_YCS_Response::HTTP_OK;
    
    protected $_error_no = 0;
    protected $_error_message = null;
    
    protected $_api_error_no = 'INTRNL';
    protected $_api_error_message = null;
    
    protected $_request_time = 0;
    protected $_request_start_time = 0;
    
    /**
     * Function to get formatted data from API response
     * 
     * @param array $data
     * @param sting $api
     * @param array $configurations
     * @param \App\Modules\Processor\YCS\Response\Processor_YCS_Security $security
     * @throws Exception
     */
    public function __construct(& $data, $api, $configurations, Processor_YCS_Security $security = null)
    {
        $this->_api = $api;
        $this->_configurations = $configurations;
        $data = HtmlCompress::compress($data);
        
        $response = $this->_send_curl_request($data);
        
        if ($this->_http_code == self::HTTP_NOT_FOUND)
        {
            throw new Exception(
                $this->_api.' - The requested URL is not found on the server'
            );
        }
        
        if($this->has_http_error())
        {
            throw new Exception(
                'API: '. $this->_api.' - '. $this->error_no() .' - '. $this->error_message()
            );
        }
        
        $formatted_response = $this->_format($response);
        
        if (empty($formatted_response['responseBody']))
        {
            throw new Exception(
                "API: '. $this->_api.'\nError No: '. $this->error_no() .'\nError Message: Processor_YCS_Response: Response is Unparsable.\n\nRequest:\n'.$data.'\n\nResponse:\n:".$response
            );
        }
        
        $this->_api_error_no = $formatted_response['responseBody']['responseCode'];
        $this->_api_error_message = $formatted_response['responseBody']['responseText'];

        if ($this->has_api_error())
        {
            throw new Exception(
                'API: '.$this->_api.' \nError No: '.$this->api_error_no().'\nError Message: '.$this->api_error_message().'\n\nRequest:\n'.$data.'\n\nResponse:\n'.$response
            );
        }
        
        $message = $this->generate_log_message($data, $response);        
        
        Logger::create('PROCESSOR/YCS/API/' . $this->_api . '/SUCCESS', [
           $message
        ]);
        
        $this->_data = $formatted_response['responseBody']['response']['ResponseXml'];
    }
    
    /**
     * Curl request to YCS API
     * 
     * @param array $data
     * @return string
     */
    protected function _send_curl_request(& $data)
    {
        $time = microtime(true);
        $headers = $this->_curl_headers;
        
        array_push($headers, 'Content-length: ' . strlen($data));
        
        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $this->_configurations['url']);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_VERBOSE, '0');
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_SSL_VERIFYHOST, '1');
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, '1');
        
        curl_setopt($request, CURLOPT_POSTFIELDS, $data);
        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        
        $response = curl_exec($request);
        
        $this->_http_code = curl_getinfo($request, CURLINFO_HTTP_CODE);
        $this->_request_time = curl_getinfo($request, CURLINFO_TOTAL_TIME);
        $this->_request_start_time = $time;
        
        $this->_error_no = curl_errno($request);
        $this->_error_message = curl_error($request);
        
        curl_close($request);
        
        Logger::create('PROCESSOR/YCS/API/RESPONSE/CURL/REQ/', [
           $this->_api,
           $time
        ]);
        
        return $response;
    }
    
    /**
     * API result after YCS call
     * 
     * @param type $data
     * @return type
     * @throws Exception
     */
    protected function _format($data)
    {
        preg_match('#<ns2:naradaResponse>(.+?)</ns2:naradaResponse>#is', $data, $matches);
        
        if (empty($matches[1]))
        {
            throw new Exception($this->_api.' - The Processor cannot be reached. Replied: '.$data);
        }
        
        $xml = new Processor_Response_XML('<api_result>' . html_entity_decode($matches[1]) . '</api_result>');
        $xml = $xml->document();
        
        return $xml['api_result'];
    }
    
    /**
     * return http_code
     */
    public function http_code()
    {
        return $this->_http_code;
    }
    
    /**
     * @return data
     */
    public function data()
    {
        return $this->_data;
    }
    
    /**
     * @return error_no
     */
    public function error_no()
    {
        return $this->_error_no;
    }
    
    /**
     * @return error_mesage
     */
    public function error_message()
    {
        return $this->_error_message;
    }

    /**
     * return api_error_no
     */
    public function api_error_no()
    {
        return $this->_api_error_no;
    }
    
    /**
     * @return api_error_message
     */
    public function api_error_message()
    {
        return $this->_api_error_message;
    }

    /**
     * @return has_api_error
     */
    public function has_api_error()
    {
        return Processor_YCS_Response::API_ERROR_OK_CODE != $this->api_error_no()
            && Processor_YCS_Response::API_ERROR_OK_MESSAGE != $this->api_error_message();
    }

    /**
     * @return has_http_error
     */
    public function has_http_error()
    {
        return !empty($this->_error_no);
    }
    
    /**
     * @return has_error
     */
    public function has_error()
    {
        return $this->has_http_error() || $this->has_api_error();
    }
    
    /**
     * @return request_time
     */
    public function request_time()
    {
        // in milliseconds
        return !empty($this->_request_time) ? ceil($this->_request_time * 1000): 0;
    }
    
    /**
     * Function to generate log message
     *  
     * @param type $data
     * @param type $response
     * @return type
     */
    public function generate_log_message($data, $response)
    {
        $message = [
            'Message' => "Request:\n" .  $data . "\n\nResponse:\n" . $response
        ];
        
        if (empty($this->_request_start_time))
        {
            return implode(",", $message);
        }
        
        $value = ceil((microtime(true) - $this->_request_start_time) * 1000);
        
        $duration = '';
        switch (true)
        {
            case $value >= 5000:
                $duration = self::DURATION_TAG_5SEC;
                break;
            case $value >= 3000 && $value < 5000:
                $duration = self::DURATION_TAG_3SEC;
                break;
            case $value >= 1000 && $value < 3000:
                $duration = self::DURATION_TAG_1SEC;
                break;
            case $value >= 500 && $value < 1000:
                $duration = self::DURATION_TAG_500MS;
                break;
            default:
                return implode(",", $message);
                break;
        }
        
        $message['Duration_Tag'] = "Duration_Tag:" . $duration;
        $message['Duration'] = "Duration:" . $value . self::UNIT_MILLISECONDS;
        
        return implode(",", $message);
    }
}