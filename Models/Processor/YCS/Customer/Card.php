<?php 
namespace App\Modules\Processor\Models\Processor\YCS\Customer;

use App\Modules\Processor\Models\Processor\YCS\Processor_YCS;

class Card
{
    protected $_card = null;
    
    /**
     * Function set card variable to card object
     *
     * @param  object card
     */
    public function with($card)
    {
        $this->_card = $card;
        return $this;
    }
    
    /**
     * Function update campaign code
     *
     * @param  object card
     * @param  intger campaign_code
     * @param  string enable
     */
    public function update_campaign_code($card = null, $campaign_code = null, $enable = 'Y')
    {
        if (empty($card))
        {
            $card = $this->_card;
        }
        
        return Processor_YCS::make('AOVRST', $card, $campaign_code, $enable)->response();
    }  
    
    /**
     * Function get card balance
     *
     * @param  object card
     */
    public function get_available_balance($card = null)
    {
        if (empty($card))
        {
            $card = $this->_card;
        }       
        
        return Processor_YCS::make('APACBL', $card)->response();
    }
    
}