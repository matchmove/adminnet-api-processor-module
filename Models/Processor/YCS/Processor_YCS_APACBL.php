<?php 
namespace App\Modules\Processor\Models\Processor\YCS;

use App\Modules\Processor\Models\Processor\YCS\Processor_YCS;
use Validator;

class Processor_YCS_APACBL extends Processor_YCS
{
    protected $_cached = false; // numeric value or false
    
    /**
     * Function to validate inputs
     * @param array $data
     * @return array
     */
    protected function _validate(Array & $data)
    {
        $rules = [
            'ProxyNumber' => 'required|digits:12',            
        ];
        
        $validator = Validator::make($data, $rules);
        
        return $validator->fails() ? $validator->errors() : true;
    }
    
    /**
     * Function to send params in YCS API
     * @param obj $card
     * @return array
     */
    public function send($card)
    {
        $map = array();
        
        $this->_configuration_group = $card->getCardDetails($card->id)->code;
        
        $map['TxnRefNo'] = $this->_get_transaction_reference_number($card->proxy_number);
        $map['ProxyNumber'] = $card->proxy_number;        
        
        return $map;
    }
}