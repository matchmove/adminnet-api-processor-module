<?php 
namespace App\Modules\Processor\Models\Processor\YCS;

use App\Modules\Processor\Models\Processor\YCS\Processor_YCS;
use Validator;

class Processor_YCS_AOVRST extends Processor_YCS
{
    protected $_cached = false; // numeric value or false
    
    protected function _validate(Array & $data)
    {
        $rules = [
            'ProxyNumber' => 'required|digits:12',
            'CampaignCode' => 'required',
            'ProductCode' => 'required',
            'Flag' => 'required|in:Y,N',
        ];
        
        $validator = Validator::make($data, $rules);
        
        return $validator->fails() ? $validator->errors() : true;
    }
    
    public function send($card, $campaign_code, $enable)
    {
        $map = array();
        
        $this->_configuration_group = $card->getCardDetails($card->card_id)->code;
        
        $map['TxnRefNo'] = $this->_get_transaction_reference_number($card->proxy_number);
        $map['ProxyNumber'] = $card->proxy_number;
        $map['CampaignCode'] = empty($campaign_code) ? $this->campaign_code() : $campaign_code;
        $map['ProductCode'] = $this->product_code();
        $map['Flag'] = strtoupper($enable);
        
        return $map;
    }
}