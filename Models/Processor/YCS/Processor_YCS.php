<?php
namespace App\Modules\Processor\Models\Processor\YCS;

use App\Modules\Processor\Models\Processor;
use App\Modules\Processor\Models\Processor\YCS\Processor_YCS_Request;
use Exception;
use App\Modules\Core\Helpers\Logger;

abstract class Processor_YCS
{
    /* Constant for Namespace */
    const CLASS_NAMESPACE = __NAMESPACE__ . '\\' .'Processor_YCS_';
    
    /* Constants for YCS config */
    protected $_configuration_name = 'processor';
    protected $_configuration_group = 'default';
    
    protected $_api = null;
    protected $_data = array();
    protected $_response = null;
    
    /**
     * Function to make YCS call
     * @param string $function
     * @return \App\Modules\Processor\YCS\class_name
     */
    public static function make($function)
    {
        $params = func_get_args();
        
        array_shift($params);
        
        // get api class name
        $class_name = Processor_YCS::CLASS_NAMESPACE . $function;
        
        // create new instance of given api class and calls constructor.
        $class = new $class_name($params);
        return $class;
    }
    
    /**
     * Constructor to call the send method and get the response from the API
     */
    public function __construct()
    {        
        if (empty($this->_api))
        {
            $this->_api = str_replace(Processor_YCS::CLASS_NAMESPACE, '', get_class($this));
        }
        
        $args = func_get_args();
        
        if (!empty($args))
        {
            $this->_data = call_user_func_array(array($this, 'send'), $args[0]);

            if (!empty($this->_data))
            {
                $this->_response = $this->_send($this->_data);
            }
        }
    }
    
    /**
     * return ycs configuration
     */
    protected function _load_configurations()
    {
        return Processor::config($this->_configuration_group);
    }

    /**
     * return reposnse
     */
    public function response()
    {
        return $this->_response;
    }
    
    /**
     * get product code from ycs config
     */
    public function product_code()
    {
        return $this->configurations('product_code');
    }
    
    /**
     * get campaign code from ycs config
     */
    public function campaign_code()
    {
        return $this->configurations('campaign_code');
    }
    
    /**
     * get currency code from ycs config
     */
    public function currency_code()
    {
        return $this->configurations('currency_code');
    }
    
    /**
     * Format's output response.
     *
     * @param  Processor_YCS_Response  $response  Response of the sent Request
     */
    protected function _format(& $response)
    {
        return $response;
    }
    
    /**
     * Function to generate transaction reference number for API call
     * 
     * @param string $proxy_id
     * @param sting $namespace
     * @return string
     */
    protected function _get_transaction_reference_number($proxy_id, $namespace = null)
    {
        // this needs to be exactly 32 length
        $id = $proxy_id . hash('crc32b', microtime(true)) . substr(uniqid(), -2);
        
        return empty($namespace)
            ? $this->_api . $id
            : hash('md5', $namespace . $this->_api . $id);
    }
    
    /**
     * Function to call YCS API and get API response
     * 
     * @param type $data
     * @return type
     * @throws Exception
     */
    protected function _send($data)
    {
        $valid = $this->_validate($data);
        
        if (true !== $valid)
        {
            $errors = $valid;

            if(!empty($errors))
            {
                throw new Exception($errors);
            }
        }
        
        $time = microtime(true);
       
        $security = null;//$this->is_secured() ? new Processor_YCS_Security($this->configurations()): null;
        
        $request = new Processor_YCS_Request($data, $this->_api, $this->configurations(), $security);
        
        $response = $request->send();
        
        $response = $this->_format($response);
        
        Logger::create('PROCESSOR/YCS/API/REQUEST/', [
            $this->_api,
            $time
        ]);
        
        return $response;
    }
    
    /**
     * Function to get configuration value for particular key like product_code 
     * 
     * @param string $index
     * @return sting
     */
    public function configurations($index = null)
    {
        $config = $this->_load_configurations();
        
        if (empty($index))
        {
            return $config;
        }
        
        $index = explode('.', $index);
        
        do
        {
            $i = array_shift($index);
            
            if (!isset($config[$i]))
            {
                return null;
            }
            
            $config = $config[$i];
            
        } while(!empty($index));
        
        return $config;
    }
}