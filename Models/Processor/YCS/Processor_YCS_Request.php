<?php 
namespace App\Modules\Processor\Models\Processor\YCS;

use App\Modules\Processor\Models\Processor\Response\Processor_Response_XML;
use App\Modules\Processor\Models\Processor\YCS\Processor_YCS_Response;
use App\Modules\Core\Helpers\HtmlCompress;
use App\Modules\Processor\Helpers\Xml_Parser;
use App\Modules\Core\Helpers\Logger;

class Processor_YCS_Request 
{
    const CACHE_DEFAULT_LIFETIME = 900;
    
    protected $_data = array();
    protected $_api = array();
    protected $_configurations = array();
    protected $_cached = Processor_YCS_Request::CACHE_DEFAULT_LIFETIME;
    protected $_cache_name_callback = null;
    
    protected $_encrypted = true;
    
    protected $_security = null;
    
    protected $_bench = array();
    
    /**
     * Set data in class variables
     * 
     * @param type $data
     * @param type $api
     * @param type $configurations
     * @param \App\Modules\Processor\YCS\Request\Processor_YCS_Security $security
     */
    public function __construct($data, $api, $configurations, Processor_YCS_Security $security = null)
    {
        $this->_data = $data;
        $this->_api = $api;
        $this->_configurations = $configurations;
        $this->_security = $security;
    }
    
    /**
     * @return ref_no
     */
    protected function _generate_ref_no()
    {
        return str_random(12);
    }
    
    /**
     * Format data for API request
     * 
     * @param type $parameters
     * @return type
     */
    protected function _format_body(& $parameters)
    {
        $xml_header = '<?xml version="1.0" encoding="utf-8"?>';
        
        $xml_parser = new Xml_Parser;
        $xml_request = $xml_parser->toXml($parameters, 'RequestXml');
        
        return trim(HtmlCompress::compress(str_replace($xml_header, '', $xml_request)));
    }
    
    /**
     * Assign values to the XML body
     * 
     * @param array $data
     * @return array
     */
    protected function _format(& $data)
    {        
        $msg_ref_no = $this->_generate_ref_no();
        $session = null;
        $token = null;
        
        $body = $this->_format_body($data);
        
        $xml = array();
        $xml['body'] = Processor_Response_XML::encode($body);
        $xml['api'] = $this->_api;
        $xml['user_id'] = $this->_configurations['user_id'];
        $xml['password'] = $this->_configurations['password'];
        $xml['client_id'] = $this->_configurations['client_id'];
        $xml['msg_ref_no'] = $msg_ref_no;
        $xml['session'] = $session;
        $xml['token'] = $token;

        \View::addNamespace('processor', app_path().'/Modules/Processor/Views');
        $view = view('processor::xml.request', $xml)->render();
        
        return $view;
    }
    
    /**
     * @return response
     */
    public function send()
    {
        $time = microtime(true);
        
        $response = $this->_execute($this->_data);
        
        Logger::create('PROCESSOR/YCS/API/RESPONSE/CURL/NET/', [
            $this->_api,
            $time
        ]);
        return $response->data();
    }

    /**
     * Execute the process of API call
     * 
     * @param type $data
     * @return Processor_YCS_Response
     */
    protected function _execute($data)
    {
        $request = $this->_format($data);
        
        $response = new Processor_YCS_Response($request, $this->_api, $this->_configurations, $this->_security);
        return $response;
    
    }
}